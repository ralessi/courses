Ewa Lenart

\textbf{Les enjeux de l'enseignement/apprentissage des langues
étrangères à l'école primaire}

Depuis les trente dernières années, on observe un engouement pour
introduire l'enseignement des langues étrangères aux jeunes enfants dès
l'école élémentaire (Enever 2011, Muñoz 2006, Nikolov et Mihaljevic
Djigunovic 2011). Le nombre de langues proposées augmente, même si
l'anglais assure son hégémonie, et l'âge du début de l'apprentissage
diminue, suivant l'idée largement répandue \emph{the earlier, the
better}. De plus en plus de pays européens, notamment la France,
introduisent cet enseignement dès le début de la scolarité obligatoire,
c'est-à-dire à l'âge de six ans environ (Muñoz 2014, Mourão \& Lourenço
2015). Les décisions en matière de politiques linguistiques et
éducatives dans ce domaine ont été influencées par les résultats de
recherches en acquisition des langues et en neurosciences (Enever 2015).
Cependant, ces recherches concernent souvent des contextes immersifs où
le nombre d'\emph{input} et sa qualité ne peuvent être comparés à la
classe de langue avec une heure et demie (en moyenne) de cours de langue
par semaine. De plus, le nombre d'élèves par classe, le curriculum (les
programmes) diffèrent, selon le contexte, et le facteur de l'âge n'est
qu'une variable parmi d'autres, comme les caractéristiques individuelles
des apprenants, leur conscience métalinguistique et inter-linguistique,
le type d'enseignement proposé (cf. Murphy 2014, par exemple) pour
caractériser la classe de langue à l'école primaire.

Dans cette communication, nous présenterons différents points de vues
sur l'enseignement/apprentissage «~précoce~» des langues et donnerons
quelques résultats d'études issues du projet \emph{PARI Primaire} sur
l'apprentissage de l'anglais à l'école primaire (cf. Hilton, Lenart et
Zoghlami 2016).

\emph{\textbf{Références}}

Enever, J. (ed.) (2011). \emph{ELLiE: Early Language Learning in
Europe}. London~: British Council.

Enever, J. (2015). The advantages and disadvantages of English as a
foreign language with young learners. In Bland, J. (ed.). \emph{Teaching
English to Young Learners: Critical Issues in Language Teaching with
3-12 Year Olds}. London: Bloomsbury Publishing, 51-70.

Hilton, H., Lenart, E. \& Zoghlami, N. (2016). Compréhension et
production en anglais L2 à l'école primaire. \emph{Revue française de
linguistique appliquée} 2 vol. XXI, 65-80.

Mourão, S. \& Lourenço, M. (eds) (2015). \emph{Early Years Second
Language Education: International Perspectives on Theory and Practice}.
New York: Routledge.

Muñoz, C. (ed.) (2006). \emph{Age and the Rate of Foreign Language
Learning}. Clevedon: Multilingual Matters.~

Muñoz, C. (2014). The interaction of L1 timing, onset age and input in
second/foreign language acquisition.\emph{~Linguistic Approaches to
Bilingualism},~4/3, 368-372.

Murphy, V.A. (2014). \emph{Second Language Learning in the Early School
Years. Trends and Contexts}. Oxford: Oxford University Press.

Nikolov, M. \& Mihaljevic Djigunovic, J. (2011). All shades of every
color: An overview of early teaching and learning of foreign languages.
\emph{Annual Review of Applied Linguistics} 3, 95-119.

Maria HELLERSTEDT

\textbf{Se mettre en position pour apprendre le suédois. }

Apprendre une langue étrangère implique parfois un changement de vision
du monde. En effet, le francophone qui apprend le suédois doit prendre
en compte les orientations des objets et des êtres vivants afin de
pouvoir parler de leur emplacement, car ce concept est encodé en
majorité par des verbes de position (\emph{sitta} « être assis »,
\emph{ligga} « être couché », \emph{stå} « être debout »). Plusieurs
études antérieures ont montré que l'acquisition de ces verbes pose un
problème aux apprenants du suédois langue étrangère (Viberg, 1985, 1998
; Hellerstedt, 2013). Enseignante de suédois aux étudiants francophones,
notre souci est donc d'élaborer une méthode afin d'augmenter la
compréhension pour la sémantique complexe de ces verbes et de faciliter
leur acquisition. Une étude pilote a été conduite mesurant la production
écrite des étudiants avant, juste après et plusieurs mois après un
enseignement détaillé et ciblé, dont les résultats seront présentés dans
cette communication.

Hellerstedt, M. 2013. \emph{L'utilisation et l'acquisition des verbes de
position en suédois L1 et L2,} Thèse de doctorat, Université de Paris 4.

Viberg, Å. 1985. "Lexikal andraspråksinlärning. Hur polsk-, spansk-och
finskspråkiga lär in svenskans placerarverb." {[}Acquisition lexicale
dans une deuxième langue. La manière dont les verbes de placement
suédois sont appris par les polonophones, les hispanophones et les
finnophones{]}In: Viberg, Å \& Axelsson, M. \emph{SUM-rapport 2.}
Stockholm : Stockholm university. 5-91.

Viberg, Å. 1998. "Crosslinguistic perspectives on lexical acquisition:
the case of language-specific semantic differentiation."In : Haastrup,
K. \& Viberg, Å. (éds.). \emph{Perspectives on lexical acquisition in a
second language. Travaux de l'institut de linguistique de Lund} 38. Lund
: Lund university press. 175-208.

***
Stéphanie GOBET~:

\textbf{« Les erreurs référentielles dans les écrits d'enfants sourds :
L2 ou système culturel} »

Notre propos porte sur l'analyse du mouvement référentiel à partir de
textes écrits par des enfants sourds. Après avoir présenté succinctement
le rapport de l'enfant sourd à l'écrit et les raisons pour lesquelles ce
dernier est à considérer comme FLE (Français Langue Étrangère) voire FLS
(Français Langue Seconde), nous expliquerons les spécificités
structurelles des LS. ~Notre communication se poursuivra par la
description du protocole et les résultats obtenus suite à l'analyse des
formes linguistiques pour les fonctions d'introduction, de maintien et
de réintroduction du référent en position de sujet.

Durant de nombreuses années, l'enfant sourd a été considéré comme un
non-entendant et non comme un locuteur d'une langue différente. En 1985,
le rapport Gillot constate que 80\% des personnes sourdes seraient
illettrées. Différents chercheurs (Cuxac, 2000, Jacob, 2001) vont alors
établir que ce pourcentage est la conséquence des méthodes
d'apprentissage du français écrit, enseigné alors comme Langue 1.
L'apprentissage de l'écriture représente une difficulté importante dans
le développement de l'enfant sourd dont la surdité est sévère ou
profonde. Pour cette population dont la langue est atypique, l'écrit est
dorénavant perçu comme une langue seconde et son apprentissage diffère
de celui des enfants entendants, le déficit auditif ne permettant pas un
apprentissage basé sur le feed-back auditif.

Notre corpus est composé de textes écrits recueilli auprès de 20 enfants
sourds sévères ou profonds dont la surdité est prélinguale (10 enfants
en classe de CM1, 10 enfants en classe de CM2), et qui sont scolarisés
dans un établissement suivant les objectifs du Bulletin Officiel pour le
français écrit L1.

Nous avons travaillé sur deux types de textes: des narrations
personnelles et des textes expositifs. Le protocole, emprunté au projet
Spencer dont le thème principal est l'aspect développemental de la
littéracie chez les enfants et les adultes dans plusieurs langues. Son
objectif est d'étudier les variations d'un texte aux niveaux
macrostructure et microstructure. Les textes étudiés ont été segmentés
en clause afin d'extraire les formes suivantes : introduction, maintien,
promotion et réintroduction du protagoniste. Les résultats obtenus ont
été comparés avec ceux d'enfants entendants. Bien que nous constations
un développement du système référentiel entre les enfants sourds en
classe de CM1 et les enfants sourds en classe de CM2, il apparaît une
grande disparité entre les enfants entendants et les enfants sourds. Par
exemple, les enfants sourds, dans le cas du maintien de la référence,
emploient principalement un pronom conjoint (32\% dans les textes
narratifs et 44\% dans les textes expositifs contre 79\% dans les textes
narratifs des enfants entendants et 19\% dans les textes expositifs de
ces mêmes enfants). Le résultat le plus significatif est l'absence de
formes référentielles dans les textes des enfants sourds, quel que soit
le type de texte.

Exemple :

2 : nous faire groupe

3 : qui faire dessins

4 : et écrire dessins.

L'emploi des différentes formes linguistiques et l'absence de formes
relèvent-ils d'un système autonome ou ne serait-ce pas la manifestation
de variations linguistiques fréquentes chez les apprenants L2 ?

\textbf{Bibliographie}

Charrow, V.R. (1981). The written english of deaf adolescents, In
Writting: The nature, development and teaching of written communication,
Whiteman, M.F, pp. 179-187,

Hillsdale, ErlbaumCuxac, C. (2000) : \emph{La Langue des Signes
Française; les Voies de l'Iconicité}, Faits de Langues n°15-16, Paris:
Ophrys.

Dubuisson, C. \& Nadeau, M. (1994). Analyse de la performance en
français écrit des apprenants sourds oralistes et gestuels, In Revue de
l'association canadienne de linguistique appliquée (ACLA), Vol. 16, n°1

Dubuisson, C. \& Daigle, D. (1998). Lecture, écriture et surdité :
visions actuelles et nouvelles perspectives, ed. Logiques, Montréal.

Jacob, S. (2001). L'écrit chez les enfants sourds : étude de la cohésion
référentielle, Mémoire de DEA, Université Lumière Lyon 2.

Lacerte, L. (1988). La langue des signes québécoise et le français :
difficulté à l'écrit chez la personne sourde, Mémoire de Maîtrise,
Université du Québec, Montréal.

Freiderikos VALETOPOULOS

\textbf{L'emploi de \emph{donc} dans un corpus écrit d'apprenants de
FLE}

Dans le cadre de cette communication, nous nous proposons d'analyser
l'emploi de la forme \emph{donc} dans un corpus écrit d'apprenants.
Après une brève présentation de la littérature concernant la fonction de
\emph{donc} dans les productions des natifs, nous étudierons les
fonctions que ce marqueur assume dans un corpus d'apprenants de niveau
intermédiaire. Ces observations seront mises en perspective par rapport
à ce qui est présenté dans les manuels FLE.

Le marqueur discursif \emph{donc} semble avoir un statut
plurifonctionnel en situation de discours. Comme l'ont déjà démontré
Bolly et Degand (2009: 28), \emph{donc} « peut, au-delà de son emploi
conséquentiel, remplir des fonctions de nature plus discursives telles
que la récapitulation, la reformulation et/ou explicitation, la
transition participative et la structuration conceptuelle». Nous nous
posons alors la question de savoir si ces usages observés dans le
discours des locuteurs natifs se reflètent dans les écrits des
apprenants du français langue étrangère.

Notre étude se concentrera sur deux corpus différents: le corpus CFLE
qui contient des productions d'apprenants qui vivent en France et qui
apprennent le français, en milieu endolingue, dans un établissement
francophone, et le corpus Hellas-FLE avec des productions d'apprenants
qui vivent et qui apprennent le français en milieu exolingue.

Bolly, C. ; Degand, L. (2009). Quelle(s) fonction(s) pour "donc" en
français oral ? : Du connecteur conséquentiel au marqueur de
structuration du discours. \emph{Lingvisticae Investigationes}, 32/1,
1-32.

Bouchard, R. (2002). \emph{Alors, donc, mais}..., « particules
énonciatives » et / ou« connecteurs »: Quelques considérations sur leur
emploi et leur acquisition. \emph{Syntaxe et sémantique}, 3(1), 63-73.
Doi :10.3917/ss.003.0063.

Hansen, M. (1997). `Alors' and `donc' in spoken French : a reanalysis.
\emph{Journal of Pragmatics}, 28--2, 153--187

Rossari, C.; Jayez, J. (1997). Connecteurs de conséquence et portée
sémantique. \emph{Cahiers de linguistique française}, 19, 233--265.

Rossari, C.; Jayez, J.. (1996). Donc et les consécutifs. Des systèmes de
contraintes différentiels. \emph{Linguisticae Investigationes} XX-1,
117--143.

Zénone, A. (1982). La consécution sans contradiction : donc, par
conséquent, alors, ainsi, aussi (1epartie). \emph{Cahiers de
linguistique française}, 4, 107--141.

Zénone, A. (1983). La consécution sans contradiction : donc, par
conséquent, alors, ainsi, aussi (2epartie). \emph{Cahiers de
linguistique française}, 5, 189--214.

Danh Thành DO HURINVILLE

\textbf{Du nom (\emph{limite}) et de l'adjectif (\emph{juste}) aux MD à
l'oral : \emph{Limite}, c'est \emph{juste} sublimissime !}

Après avoir brièvement rappelé que \emph{limite} est un nom (emprunté au
latin \emph{limes}, \emph{limitis}, XIVe siècle : « \emph{Limite} d'une
ville ») et que \emph{juste} est un adjectif (issu du latin
\emph{justus}, XIIe siècle : « Il faut être \emph{juste} avant d'être
généreux »), cette communication a pour objectif de retracer l'évolution
de ces deux lexèmes vers un emploi grammatical (« Une personne
anti-bling bling \emph{limite} austère » et « Le café est \emph{juste}
chaud »),puis d'examiner leur emploi comme des MD (ou pragmatèmes)
utilisés exclusivement à l'oral (« \emph{Limite}, ce film est
\emph{juste} génialissime ! » ou « Ce film est \emph{juste}
génialissime, \emph{limite} ! »). Dans ces exemples, limite pouvant être
placé aussi bien en début qu'en fin d'énoncé, se comporte comme un MD,
alors que \emph{juste} fonctionne comme un adverbe d'attitude
énonciative émettant le point de vue du locuteur vis-à-vis de l'énoncé
et de l'allocutaire, pouvant être assimilé à un MD.

Rappelons que les lexèmes (noms, verbes, adjectifs) et les grammèmes
(adverbes, marqueurs aspectuels, prépositions, conjonctions),
participant au contenu propositionnel de l'énoncé, assumant tous les
deux des rôles intraphrastiques, peuvent être utilisés aussi bien à
l'écrit qu'à l'oral, tandis que les pragmatèmes (marqueurs discursifs,
interjections) ne participant pas au contenu de l'énoncé, jouent des
rôles sur le plan macro-textuel et remplissent des fonctions
pragma-sémantiques qui consistent notamment à lier des actes
illocutoires, à réaliser des actes illocutoires, ou à manifester son
écoute. Par conséquent, leur rôle se situe, non pas sur le plan
référentiel (c'est le rôle des lexèmes et des grammèmes), mais sur le
plan communicatif. Les pragmatèmes ne fusionnent pas avec les autres
constituants des énoncés : ils sont séparés par une virgule à l'écrit ou
par une pause à l'oral (leur portée étant extraphrastique, sur
l'ensemble de l'énoncé) ; ils ne suivent donc pas la même courbe
prosodique que le reste de l'énoncé.

Examinons maintenant deux parcours illustrés respectivement par
\emph{limite} et par \emph{juste}, qui se comportent de nos jours comme
des MD à l'oral.

\textbf{1/ De la locution pragmatique à la limite au MD \emph{limite} }

\begin{quote}
(1) \emph{~\textbf{À la limite}, ce film est juste génialissime !
(oral)}

(2) ~\emph{Ce film est juste génialissime, \textbf{à la limite} !
(oral)}

(3) \emph{~\textbf{Limite}, ce film est juste génialissime ! (oral)}

(4) ~\emph{Ce film est juste génialissime, \textbf{limite} ! (oral)}

Si la locution \emph{à la limite} suivie ou précédée d'une proposition
(ex. 1 et 2), signifiant « à la rigueur, au pire des cas », relevant de
la modalité d'énonciation, instaurant une relation entre le locuteur et
l'allocutaire, est une forme complète enregistrée depuis longtemps dans
les dictionnaires, \emph{limite} suivi ou précédé également d'une
proposition (ex. 3 et 4) est une forme tronquée, qui n'est utilisée qu'à
l'oral et n'est encore reconnue par aucun dictionnaire. Dans ce
contexte, \emph{limite}, qui remplace la locution pragmatique \emph{à la
limite}, se comporte comme un MD pouvant être placé aussi bien en début
qu'en fin d'énoncé.
\end{quote}

\textbf{2/ \emph{Juste}, de l'adverbe de verbe à l'adverbe d'attitude
énonciative (à double modalisation)}

(5a) Le café est \textbf{Ø} chaud. (5b) Le café est \textbf{très} chaud.
(5c) Le café est \textbf{juste} chaud.

(6a) Ce film est \textbf{Ø} génial (magnifique, merveilleux, sublime,
formidable) !

(6b) Ce film est *\textbf{très} génial (magnifique, merveilleux,
sublime, formidable) !

(6c) Ce film est \textbf{juste} génial (magnifique, merveilleux,
sublime, formidable) !

Dans les exemples ci-dessus, le fonctionnement et l'interprétation de
\emph{juste} sont différents, selon que cet adverbe est antéposé à des
adjectifs gradables comme \emph{chaud} en (5c), ou à des adjectifs
intensifs (exprimant un très haut degré) comme \emph{génial, magnifique,
merveilleux, sublime, formidable} en (6c). Lorsque \emph{juste} est
antéposé aux adjectifs gradables, il fonctionne comme un adverbe de
verbe, signifiant, d'après le PRLF, «restriction» ou «manière trop
stricte», et ne porte que sur ces adjectifs gradables dont il module
l'intensité. Dans (5a), le locuteur ne fait qu'asserter un état : « être
chaud », tandis que dans (5b), celui-ci est modulé par l'adverbe de
degré \emph{très}. Dans (5c), cet état directement modulé par
\emph{juste} se situe à peine en dessous de celui sans \emph{juste} en
(5a). Observons maintenant (6a), (6b) et (6c). Lorsque \emph{juste} est
antéposé aux adjectifs intensifs en (6c), il fonctionne comme un adverbe
d'attitude énonciative, à double modalisation, sur l'adjectif et sur
l'énonciation comme suit :

\begin{enumerate}
\def\labelenumi{(\roman{enumi})}
\item
  Modalisation sur l'adjectif superlatif de l'énoncé : \emph{juste} met
  en relief l'intensité inhérente à cet adjectif. Il s'agit donc d'un
  effet de loupe sur celui-ci (ou effet « hyperbolique », cf. Salvan,
  2015).
\item
  Modalisation sur l'énonciation : le locuteur souligne la justesse et
  la pertinence de la sélection de cet adjectif.
\end{enumerate}

Les adjectifs dans (6c), tous sémantiquement intensifs, n'ont besoin
d'être modulés par aucun adverbe de degré, ce qui explique la
non-recevabilité de \emph{très} dans (6b). En recourant à (6c), le
locuteur envoie un message subliminal, incitant l'allocutaire à accepter
son jugement, ce qui n'est pas le cas de l'énoncé sans \emph{juste} dans
(6a). Avec ces adjectifs, le locuteur empêche, par anticipation,
l'allocutaire de contester ce choix judicieux : le locuteur fait
comprendre qu'il n'exagère pas car il estime être dans la juste mesure.
Ce recours à \emph{juste} est donc interprété comme un emploi
\textbf{métalinguistique}, permettant au locuteur d'émettre un jugement
à l'égard de l'allocutaire. On peut dire par ailleurs que \emph{juste}
est un marqueur de strict centrage portant sur la valeur haute retenue
(représentée par un adjectif intensif) excluant de retenir d'autres
valeurs plus faibles, et qu'il peut fonctionner comme un marqueur de
focalisation en engendrant un effet de loupe, ou effet emphatique, ou
effet « hyperbolique », sur l'adjectif intensif sélectionné. C'est dans
(6c) que \emph{juste} en tant qu'adverbe d'attitude énonciative peut
fonctionner comme un pragmatème ou un MD.

Le comportement de \emph{limite} et de \emph{juste} semble illustrer le
point de vue de Dostie et Pusch (2007), qui soulignent que les MD (ou
pragmatèmes), compte tenu de leurs caractères « éminemment oraux », «
doivent être envisagés dans un tout autre cadre, celui de la langue
orale, où la coprésence de l'interlocuteur influence la façon dont le
locuteur construit son discours », et qu'«ils apparaissent à des
endroits stratégiques et contribuent à rendre efficaces les échanges
conversationnels, ainsi qu'à aider l'interlocuteur à décoder la façon
dont le locuteur conçoit le sens purement propositionnel exprimé et se
positionne par rapport à celui-ci ».

\textbf{Bibliographie~: }

Do-Hurinville D. T., 2018, « \emph{Juste}, de l'adjectif à l'adverbe
d'énonciation, une unité juste transcatégorielle », in Céline Vaguer
(éds), Mélanges offerts à Danielle Leeman, \emph{Quand les formes
prennent sens. Grammaire, prépositions, constructions, système}, Paris,
Lambert Lucas, pp. 235-246.

Do-Hurinville D. T. \& Dao H. L., 2016, « La transcatégorialité. Une
histoire de \emph{limite} sans limite », \emph{Bulletin de la Société de
linguistique de Paris} 111, 1, pp. 157-211.

Dostie G., 2004, \emph{Pragmaticalisation et marqueurs discursifs.
Analyse sémantique et traitement lexicographique}, Bruxelles, De Boeck,
Duculot.

Dostie G. \& Pusch C. D., 2007, « Présentation : les marqueurs
discursifs », in \emph{Les marqueurs discursifs}, G. Dostie \& C. Pusch
(éds.), \emph{Langue française} 154, pp. 3-12.

Leeman D., 2004, « L'emploi de \emph{juste} comme adverbe d'énonciation
», \emph{Langue française} 142, pp.17-30.

Salvan G., 2014, « \emph{Juste la fin du monde}. L'excès juste, ou
l'hyperbole exagère-t-elle toujours ? », \emph{Travaux neuchâtelois de
linguistique} (TRANEL) 61/62, pp.63-78.

Jeanne VIGNERON-BOSBACH

Jeanne Vigneron-Bosbach

Crisco - Forellis

Université de Caen Normandie

\textbf{\emph{Genre}, du nom au marqueur : état d'un parcours}

\emph{Genre} fait partie des mots de la langue française qui ont subi un
changement linguistique, puisque des emplois très divers coexistent en
synchronie. Il s'agit en effet d'un terme qui peut apparaître aussi bien
comme nom tête d'un syntagme nominal comme en (1) ou en (2), comme
marqueur de reformulation comme en (3), ou comme introducteur de
discours direct comme en (4) :

\emph{(1) Toutes les roses appartiennent au \textbf{genre} rosa. » ~(Le
Petit Robert, 2010).}

\emph{(2) donc euh j'suis rentrée à l'école normale des Batignolles pour
un an et puis après j'suis partie euh:: passer {[}...{]} j'suis
\textbf{l'genre de} fille: qu'a fait un + une fin d'étude à dix-huit ans
et demi quoi(oral, CFPP, 11-02)}

\emph{(3) non mais je s- j'me rappelle plus mais c'est vrai que ça a été
enfin j'trouve que c'était jeune enfin dès dès la troisième dès début
troisième non moi c'était beaucoup plus tard \textbf{genre} première
quelque chose comme ça + + (mh) (oral, CFPP, 11 04 spk2)}

\emph{(4) je lui ai bien dit les trucs euh hyper euh \textbar{} secs euh
\textbf{genre} moi j'ai plus aucun sentiment pour toi c'est terminé euh
(oral, OFROM, unine08-vwa)}

En (1) et (2), \emph{genre} se comporte encore morpho-syntaxiquement
comme un nom. En (3) et (4) en revanche, il est difficilement associable
à la catégorie nominale, puisqu'il ne peut pas être porteur d'une
détermination ni modifié par un adjectif, ni porter un morphème de
pluriel :

(2)a. *c'était beaucoup plus tard \textbf{un/le genre} première quelque
chose comme ça

(2)b. *c'était beaucoup plus tard \textbf{genre} lycéen première quelque
chose comme ça

(2)c. *c'était beaucoup plus tard \textbf{genres} première quelque chose
comme ça

Ces emplois sont surtout présents dans des productions orales présentant
un faible degré de planification, et sont plus récents comme le montre
ce tableau comparatif entre le corpus ESLO 1(1969-1974) et ESLO 2 (2014)
:

\begin{longtable}[]{@{}llll@{}}
\toprule
& Nombre total d'occurrences de \emph{genre} & Nombre d'occurrences de
\emph{genre} grammaticalisé & Genre grammaticalisé, en
pourcentage\tabularnewline
\midrule
\endhead
ESLO1 & 826 & 46 & 5,6 \%\tabularnewline
ESLO2 & 423 & 213 & 50,4 \%\tabularnewline
\bottomrule
\end{longtable}

Tableau: Comparaison du nombre d'occurrences de genre dans ESLO1 et
ESLO2

Nous pensons, à la suite de Rosier (2002), que \emph{genre} a subi un
processus de grammaticalisation. Ce processus est défini comme suit par
Marchello-Nizia (elle mentionne d'ailleurs le cas de \emph{genre}) :

\begin{quote}
On nomme 'grammaticalisation' un type de changement linguistique très
répandu dans toutes les langues du monde. On a coutume, dès l'origine,
de le décrire par un résultat : c'est le processus par lequel des
lexèmes deviennent des morphèmes. Ces nouvelles unités grammaticales
servent soit à coder des relations qui n'étaient pas codées
grammaticalement auparavant, soit qui l'étaient mais différemment (...).
{[}...{]}d'autres phénomènes tels que l'apparition de l'article défini
dans une langue qui ne le possédait pas, ou l'introduction d'un morphème
tel que \emph{genre} (qui peut construire un nom, un adjectif, ou même
une proposition en français actuel) sont des exemples de codage de
relations qui n'étaient pas exprimées grammaticalement jusqu'alors.
(Marchello-Nizia, 2009 : 15)
\end{quote}

En tant que nom, il est un bon candidat à la grammaticalisation
puisqu'on part d'une unité source lexicale pour aller vers une unité
cible grammaticale.

Afin d'appréhender cette évolution, nous avons observé son comportement
à travers ses emplois dans des corpus de français parlé présentant un
faible degré de planification. Ces corpus s'étalant sur une période
assez courte (années 1990-2010), il s'agit d'hypothèses à partir d'une
observation synchronique. Selon nous, l'évolution de ce terme se fait en
plusieurs étapes. Dans un premier temps, le terme \emph{genre} existe en
tant que nom mais présente des caractéristiques syntaxiques et
sémantiques un peu particulières, sans doute propices à un changement
linguistique. Il s'apparente alors à ce que Blanche-Benveniste et al.
appellent~« verrues nominales » (1990 : 110) : il s'agit de structures
dans lesquelles des noms comme \emph{espèce}, \emph{genre} et
\emph{sorte} sont des éléments qui « se surajoutent sur le lexique
nominal, une fois que celui-ci est développé », c'est-à-dire que le
contenu lexical principal est assumé par le nom qui suit \emph{genre}.
C'est le cas dans des exemples comme (2), que nous analyserons. ~

Dans un second temps, ce mot apparaît également dans des locutions
figées \emph{dans le genre} et \emph{du genre} où ses propriétés
nominales s'affaiblissent.

\emph{(5) Dans le \textbf{genre} pénible, il est champion (écrit,
Orsenna, Le Petit Robert, 2010)}

\emph{(6) y a eu à Pontoi- rue de Pontoise aussi + mais alors qui est
aussi hors de prix + parce que ils font + aussi euh + truc \textbf{du
genre} euh + gymnase club quoi (oral, CFPP, 05-01)}

Ces locutions constituent selon nous une deuxième étape de la
grammaticalisation de \emph{genre}. Elles sont appelées « locutions
prépositionnelles » par Danon-Boileau et Morel (1997), de même que par
Rosier (2000, 2002), ce qui suppose que ces ensembles se comportent
syntaxiquement comme une préposition.

Enfin, il apparaît seul et ne présente alors plus aucune caractéristique
nominale :

\emph{(7) est-ce que vous avez d'autres livres \textbf{genre} euh art
d'écrire ou de parler art de savoir-vivre ? (B,oral, ESLO1)}

Après avoir rapidement défini ce que l'on entend par grammaticalisation,
nous verrons tout d'abord en quoi \emph{genre} est un bon candidat à ce
type de changement. A partir de l'observation du mot \emph{genre} dans
des corpus de français parlé, nous proposerons un schéma d'évolution de
ce terme, en observant ses emplois de nom standard, puis en démontrant
que le nom \emph{genre} peut présenter déjà des caractéristiques
syntaxiques particulières, en nous intéressant ensuite à ses emplois
dans des locutions figées comme \emph{dans le genre}, \emph{du genre},
et enfin en envisageant le passage à ses emplois entant que marqueur.

\textbf{Bibliographie indicative}

BLANCHE-BENVENISTE , Claire et al., 1990, \emph{Le Français parlé :
Etudes grammaticales}, Paris :CNRS.

BLANCHE-BENVENISTE , Claire, MARTIN , Philippe, 2010, \emph{Le français
-- Usages de la langue parlée}, Paris, Louvain : Peeters.

DANON-BOILEAU, Laurent, MOREL, Mary-Annick, 1997, « \emph{Question,
point de vue, genre, style} : les noms prépositionnels en français
contemporain », Faits de langue n°9, 192-200.

DOSTIE, Gaétane, 2004, \emph{Pragmaticalisation et marqueurs discursifs.
Analyse sémantique et traitement lexicographique}, Bruxelles : De Boeck
Duculot.

LABRECQUE, Nicole, DOSTIE, Gaétane, 1996, « \emph{Cas, exemple, façon,
manière} : des cas exemplaires de polysémie », in Khadiyatoulah FALL ,
Jean-Marcel LÉARD , Paul SIBLOT (éds.),\emph{Polysémie et construction
du sens}, Montpellier : Praxiling, 171-180.

MARCHELLO-NIZIA, Christiane, 2006, \emph{Grammaticalisation et
changement linguistique}. Bruxelles : De Boeck Duculot.

ROSIER, Laurence, 2000, « Les petits ``rapporteurs'' de discours », in
Paulo de CARVALHO,Laurence LABRUNE (éds.), \emph{Grammaticalisation 1.
(dé)motivation et contrainte}, Rennes : Presses Universitaires de
Rennes, 209-229.

ROSIER, Laurence, 2002, « \emph{Genre} : le nuancier de sa
grammaticalisation », \emph{Travaux de linguistique} n° 44, 79-88.

VIGNERON-BOSBACH, Jeanne, A paraître, « Des marqueurs de (re)formulation
», actes du colloque « La Reformulation » juin 2017, Université d'
Uppsala, Suède.

- 2016, \emph{Analyse contrastive des marqueurs genre en français, like
en anglais et so en allemand dans des corpus d'oral et d'écrit
présentant un faible degré de planification}, Doctorat de linguistique
en co-tutelle, sous la direction de Sylvie Hanote (Université de
Poitiers) et Hermine Penz (Université Karl-Franz de Graz, Autriche).
Université de Poitiers.

- 2015, « Des spécialistes du (discours) direct ? », E-rea {[}En
ligne{]}, 12.2 \textbar{} 2015, mis en ligne le 15juin 2015, URL :
http://erea.revues.org/4375 ; DOI : 10.4000/erea.4375, 2014.

Antonin BRUNET~:

\textbf{Plaidoyer pour l'enseignement des marqueurs discursifs en classe
de FLE }

\textbf{Un outil indispensable pour les apprenants de niveau avancé.}

Antonin Brunet - FoReLLIS (EA3816) - Université de Poitiers

Dans les ouvrages de description de la langue tels que les grammaires ou
les dictionnaires, on constate que les marqueurs discursifs (MD) ne sont
pas reconnus en tant que catégorie spécifique (Paillard \& Ngan, 2012).
Si ces derniers ont certes fait l'objet d'un intérêt grandissant depuis
les années 80 du côté de la recherche, ils ont également fait l'objet
d'appellations (particules énonciatives / discursives, ponctuants,
connecteurs, mots du discours, marqueurs pragmatiques...) et de
descriptions variées qui ont en partie eu pour effet d'entretenir un
flou non seulement autour de leur définition mais également autour de
leur(s) utilité(s) (Delahaie, 2011; Dostie \& Pusch, 2007). Selon nous,
ce constat est directement lié à l'aspect historique de
l'institutionnalisation de la langue française où l'écrit a longtemps
été « le seul lieu de réflexion métalinguistique » (Delahaie, 2008) et
le seul repère pour l'établissement de la norme. Nous pensons que cet
écart à la norme écrite engendre certaines incompréhensions autour des
MD, qui peuvent être boudés de certains locuteurs qui les considèrent
comme trop peu formels, vides de sens ou même parasites lorsque ceux-ci
vont jusqu'à relever du « tic de langage » (Bouchard, 2002). Pour
autant, il est impossible de nier que ces derniers sont omniprésents
dans l'usage et il nous semble bien peu probable que les usagers d'une
langue puissent autant avoir recours à une unité qui serait vide de
sens. C'est pourquoi d'après Dostie \& Pusch (2007), pour comprendre
concrètement les MD et leurs utilités, il faut les étudier dans un autre
contexte : celui de la langue orale, où le discours se construit de
manière plus spontanée et sous l'influence de la présence d'un
interlocuteur.

Sur le plan didactique, les mêmes constats s'opèrent. Les outils
pédagogiques ne proposent pas un enseignement des MD en tant que
catégorie spécifique, les commentaires liés à leurs utilités sont
succincts et très généraux et ils sont souvent proposés sous forme de
listes (comprenant également des mots d'autres natures) qui ne
permettent qu'une appropriation très partielle et limitée (Paillard \&
Ngan, 2012; Delahaie, 2011). Ainsi, du fait des constats présentés
ci-dessus et de cette représentation timide dans les outils
pédagogiques, nous pensons que la plupart des enseignants de FLE ne se
représentent pas les MD comme un objet d'enseignement non seulement
légitime mais également utile pour les apprenants.

Lors de cette communication, nous envisageons d'étudier l'utilité
sémantique des MD à l'aide du corpus AvEx-FLE constitué au laboratoire
FoReLLIS dans le cadre de notre thèse de doctorat. En reprenant notre
corpus de référence constitué de productions orales continues (de type
argumentatif) de locuteurs natifs francophones, nous mènerons des
analyses qualitatives sur l'emploi de plusieurs MD (après, enfin, donc,
en fait...) par ces locuteurs et dégagerons leurs différentes utilités
sémantiques notamment en ce qui concerne leur participation dans la
construction de la cohérence et de la cohésion du discours. Nous
espérons ainsi démontrer non seulement que les locuteurs natifs en ont
une utilité raisonnée et sémantiquement constructive, mais également que
ce sémantisme justifie l'enseignement des MD en classe de FLE notamment
pour les apprenants de niveau avancé qui sont régulièrement amenés à
construire des productions similaires.

DELAHAIE. J. 2008. \emph{Français parlés et français enseignés. Analyses
linguistiques et didactiques de discours de natifs, de non-natifs et
d'enseignants.} Université de Nanterre -Paris X, 2008. Français.
\textless{}tel-00787789\textgreater{}

DELAHAIE. J. 2011. « Les marqueurs discursifs, un objet d'enseignement
pertinent pour les étudiants Erasmus? » \emph{in Études de Linguistique
Appliquée} \emph{n°162}. pp 153-163.

DOSTIE. G \& PUSCH. C. D. 2007. « Présentation. Les marqueurs
discursifs. Sens et variations. » \emph{in Langue Française n° 154}. pp.
3-12

PAILLARD. D \& NGAN. V. T. (dir.). 2012. \emph{Inventaire raisonné des
marqueurs discursifs du français. Description. Comparaison. Didactique.}
Éditions de l'Université nationale de Hanoï: Vietnam

Laurie DEKHISSI, Efi LAMPROU

\textbf{Analyse contrastive des marqueurs discursifs dans le discours
argumentatif de natifs et non natifs. Analyse sur corpus. }

La syntaxe de l'écrit et de l'oral ne peut pas être mise en opposition
de façon caricaturale (Cappeau, 2005) bien que des différences soient à
noter lorsqu'on étudie de courts extraits de genre variés.Des phénomènes
tels que le recours à la forme courte de la négation et aux dislocations
sont bien connus à l'oral (Blanche-Benveniste, 1997) alors que l'écrit
bannit ces formes, contraint par la norme du « bon usage ».

Dans cette communication, nous nous intéresserons aux marqueurs
discursifs (MD), marqueurs structurants du discours qui assurent la
cohésion et la cohérence soit au sein d'un tour de parole soit à la
jonction de deux tours de parole(Traverso, 1999).

A partir d'un corpus de productions argumentatives monologales de
locuteurs natifs et allophones de niveau avancé nous analyserons
l'emploi des marqueurs discursifs dans ce type de production.

Nos hypothèses de départ sont les suivantes :

1) Les locuteurs natifs n'utilisent pas les mêmes marqueurs discursifs
que les apprenants allophones à l'oral (Delahaie,2011)

2) Les allophones utilisent plutôt les marqueurs discursifs de
l'écrit,mais pas ceux de l'oral dû au genre argumentatif qui rappelle la
dissertation et le poids de la norme (Rançon et Dekhissi, 2017).

En effet, les apprenants de FLE vivant en France depuis un certain temps
se familiarisent rapidement avec le français parlé dont les normes
diffèrent de celle(s) de l'écrit selon le contexte. Ils prennent
conscience de cette distinction oral/écrit mais sont souvent incapables
par la suite de distinguer ce qui relève de l'un ou de l'autre en
situation académique. Ainsi, nous examinerons si les marqueurs
discursifs présents dans les productions relèvent plutôt de l'écrit ou
de l'oral et s'ils ont les mêmes fonctions communicatives en fonction du
type de locuteurs (natifs ou non natifs).

Blanche-Benveniste C. 1997."La notion de variation syntaxique dans la
langue parlée", \emph{Langue française} 115, La variation en syntaxe,
pp.19-29

Cappeau, P. 2005. « Construire une problématique de l'oral », Séminaire
doctoral de Sciences du langage organisé par Françoise Gadet Université
de Paris X - Nanterre

Delahaie, J.2011. Transposition didactique de l'inventaire raisonné des
marqueurs discursifs du français. \emph{Agence universitaire de la
francophonie}, 2011.

Rançon, J et Dekhissi, L.2017. La dissertation générale. Un objet
d'enseignement pertinent pour les apprenants ERASMUS ? \emph{Le Langage
et l'Homme}, Cortil-Wodon : EME Traverso, V.1999. L'analyse des
conversations, Paris, Nathan, coll. Linguistique 128, n° 226
