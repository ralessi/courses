\documentclass[12pt,a4paper]{article}

\usepackage[french]{babel}
\usepackage[small]{titlesec}

\usepackage{fontspec}
\usepackage{microtype}
\usepackage{ebgaramond}

\usepackage{tocdata}
\usepackage{csquotes}

\usepackage[style=authoryear]{biblatex}
\addbibresource{exercice.bib}

\begin{document}
\sectionauthor*{Esquisse d'une cartographie des dictions françaises,
  1890-1965: ruptures manifestes et filiations
  souterraines}{Marion}{Chénetier-Alev}

Tenter une cartographie des dictions des acteurs français d'une partie
du xx\textsuperscript{e} siècle, c'est s'aventurer sur un versant
d'une histoire des acteurs en France, qui n'est pas encore
écrite. C'est en outre être confronté aux problèmes méthodologiques
que soulève toute entreprise historique, encore aggravés par la
situation particulière des recherches portant sur le travail de
l'acteur. Dans cette perspective, une histoire de la \enquote{diction}
s'avère moins aisée encore. Jusqu'au début du xx\textsuperscript{e}
siècle, aucun enregistrement ne vient remplir la fonction des tableaux
puis de la photographie, et les mots mêmes font défaut pour décrire
efficacement les voix et les modes du parler au théâtre. Lorsqu'en
1907, au laboratoire du Collège de France, le phonéticien Eugène
Landry \autocite{landry1911} s'efforce d'enregistrer quelques-uns des plus
célèbres comédiens de son temps en vue de caractériser leur diction,
s'il parvient à une \enquote{radiographie} remarquablement précise des
interprétations des acteurs du Français, on est en revanche frappé du
contraste entre la rigueur de ses analyses techniques et le vide
acoustique qu'elles créent dans l'esprit du lecteur: à moins qu'il
n'ait déjà entendu ces acteurs, la lecture du diagnostic est
absolument impuissante à susciter en lui une quelconque \enquote{image
  sonore} de leur jeu.

Le minutieux travail de Landry aboutit pourtant à ce qu'il nomme une
\enquote{esthétique technique}. De fait, puisque l'on dispose de
nombreux enregistrements de comédiens sur l'ensemble du
xx\textsuperscript{e} siècle, et dès lors que les phénomènes vocaux et
élocutoires peuvent être exactement désignés, leur comparaison
systématique permettrait d'établir un schéma des continuités, des
ruptures et des évolutions de la diction, en repérant et datant par
exemple la disparition du \enquote{r} roulé, du \enquote{l} mouillé,
de la sonorisation des doubles consonnes, l'appauvrissement des
voyelles, l'accélération du débit, ou au contraire la résistance de
certaines intonations, etc. Grâce au traitement informatique de la
parole, les variations de hauteur, d'intensité et de durée, si
difficiles à démêler, peuvent enfin être différenciées. {[}\ldots{}{]}
Les critiques écrites autrefois, après une simple écoute, peuvent être
aujourd'hui passées au crible des enregistrements existants et révéler
ainsi les goûts et l'esthétique d'une époque. En somme, \enquote{une
  histoire de la diction et de son esthétique est enfin
  possible\footcite[69-81]{martinez2001}.}.

Aucune de ces mesures, cependant, ne se révèle utile pour comprendre
la qualité d'une diction, la justesse d'une interprétation. Landry ne
s'en cache pas, alors même que ses analyses très fines font souvent
état d'aspects essentiels du jeu vocal tels que la conduite de
l'énergie ou le choix des moyens en fonction des émotions à produire:
\enquote{Mais le rapport au sentiment exprimé échappe à l'analyse et
  c'est ce rapport qui fait le prix de cet art\footcite[Cf.][343]{martinez2001}}.

Aussi mon projet est-il de cheminer parallèlement à ces approches
techniques, en prenant comme point de départ l'idée généralement
admise qu'un tournant dans la diction se serait opéré au moment de la
Seconde Guerre mondiale, se traduisant par une rupture, la perte d'une
\enquote{maîtrise}, notamment celle de la diction des
classiques. Est-il possible d'affiner cette vue d'ensemble, pour
éclaircir ce qui a pu susciter ce sentiment de rupture, quelles en
seraient les causes? Que sous-entend cette lecture de l'histoire
théâtrale en termes de perte?  Peut-on poser quelques jalons sur la
carte des dictions et tenter de formuler ce qui s'y joue? L'ambition
(sinon l'utopie) de cette recherche, qui croise la vaste problématique
des rapports de la scène aux textes et aux voix des acteurs, passe de
loin le cadre d'un article.  Nous n'en proposerons ici qu'une première
étape, sous la forme de quelques entrées indiquant les voies en cours
d'exploration. Le choix d'un parcours mêlé entre espace et temps,
reflété par mon titre, s'explique par un constat fait dès les premiers
repérages: il n'y a pas une, mais des dictions en présence, qui
dépendent en effet des lieux où se déroulent les représentations comme
du genre des pièces et des médias employés, et qui se chevauchent dans
le temps -- sans compter que de manière générale différentes dictions
peuvent être le fait d'un même comédien. Enfin cette étude s'appuie,
outre les sources académiques d'usage concernant l'acteur, sur
l'écoute intensive des archives audio de l'INA\footnote{Que mesdames
  Christine Barbier-Bouvet, ancienne responsable du service Inathèque,
  et Géraldine Poels, responsable de la valorisation scientifique à
  l'INA, trouvent à nouveau ici l'expression de ma vive reconnaissance
  pour m'avoir généreusement donné accès aux archives audiovisuelles.}
et sur une collecte d'informations effectuée directement auprès de
comédiens par une série d'entretiens menés depuis 2013.

\subsection*{Les mutations phonétiques du xx\textsuperscript{e}
  siècle}

Les modes de la diction théâtrale sont indissociables des mutations
phonétiques qui affectent le xxe siècle français d'une manière
accélérée. Sans entrer dans le détail des travaux des linguistes,
indiquons d'après Pierre Léon et Michel Billières ***(Billières) trois
grandes vagues qui modèlent le siècle étudié. Avant la Seconde Guerre
mondiale, la norme phonétique pour la prononciation du français est
représentée par la grande ou moyenne bourgeoisie parisienne, qui
n'hésite pas à prendre des cours de diction comme les acteurs. Dans
les années suivant la guerre, les Français qui montent s'établir à
Paris font de la capitale un creuset où l'on assiste à un important
brassage des \enquote{accents}. Ainsi émerge au début des années 1960
un nouveau modèle appelé le \enquote{Français standard} que définit
Pierre Léon en 1968 ***(Léon, 1968, 68‑72):

Il existe une prononciation standard dont le niveau moyen est grosso
modo représenté par les annonceurs et les interviewers de la radio.
[\ldots] Leur prononciation reflète l'usage moyen, sans
recherche (pour plaire au grand public) et sans familiarité (à cause
du micro).  [\ldots] c'est le modèle proposé à longueur de
journée à des millions de Français [\ldots]

Rappelons qu'il y a treize millions de postes de télévision vendus en
1973. Les journalistes de radio-télévision \enquote{sont sélectionnés
  entre autres critères en vertu de la qualité de leur diction et de
  la clarté de leur prononciation\footnote{Cf.  ***Billières.}}.
Enfin, après une première phase de nivellement de la prononciation au
sein des couches sociales ascendantes à la fin des années 1970, une
nouvelle norme apparaît à l'orée du xxi\textsuperscript{e} siècle,
\enquote{le français de référence}: l'accent dit \enquote{parisien}
n'est plus le fait de Paris mais tend à se propager dans beaucoup de
grandes villes situées au nord de la Loire.

On y observe ce nivellement de la prononciation sans qu'il soit
vraiment possible de le rattacher à une classe sociale précise comme
c'était encore le cas pour la norme précédente. Les spécialistes
analysant cette tendance l'attribuent à l'influence des professionnels
de la parole publique [\ldots]. C'est un accent
passe-partout\footnote{Cf. ***Léon, 1968.}.

L'écoute des archives de l'INA permet de corroborer cette évolution
linguistique chez les acteurs. Mais elle fait aussi ressortir que
leurs dictions, autant que par les normes phonétiques, s'affilient ou
divergent selon d'autres critères, tels les schémas intonatifs, le
débit de la parole, la netteté de l'articulation; et que les
influences sont d'ordre individuel (la transmission se fait de un à
un) beaucoup plus que collectif. Néanmoins, la question des accents
occupe une place importante dans l'histoire des dictions et l'on ne
s'étonne pas que la première grande réforme en la matière ait partie
liée avec eux.

\printbibliography
\end{document}