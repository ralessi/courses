#!/bin/bash

# création du compteur de fichiers copiés.
countfiles=0

# On demande quels fichiers doivent être copiés. Mais avant cela, on
# explique à l'utilisateur ce que fait ce programme.
echo "Attention: ce programme copie au maximum 10 fichiers."
read -p 'Que souhaitez-vous copier: ' files

# On demande la destination:
read -p 'Répertoire de destination: ' dest

# <destination> doit être un répertoire:
if [ ! -d $dest ]
then
    echo "Erreur: la destination doit être un répertoire."
    echo "        Le cas échéant, utilisez \"mkdir $dest\""
    echo "        pour créer le répertoire de destination."
    exit 1
else # Si <destination> est un répertoire, alors pour chaque fichier
     # copié on incrémente le compteur. Et dès que le compteur
     # atteint le chiffre de 10, on sort de la boucle.
    for file in $files # pour chaque fichier à copier
    do
	if [ ! -e $file ] # si le fichier à copier n'exite pas
	then
	    echo "création de $file qui n'existe pas..."
	    touch $dest/$file
	    continue # arrêter ici et reprendre à la l. 24
	fi
        cp $file $dest
        let "countfiles = countfiles + 1"
        if [ $countfiles -eq 10 ]
        then
            break # sortie de la boucle
        fi
    done
fi

echo "Terminé. 10 fichiers au maximum ont été copiés dans $dest."
