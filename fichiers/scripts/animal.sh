#!/bin/bash

echo "Entre le chien et le chat, quel est votre animal préféré?"
read -p 'Saisissez votre réponse: ' animal

case $animal in
  [Cc]hien)
  echo "Vous avez bien raison, le chien est le meilleur ami de l'homme."
  ;;
  [Cc]hat)
  echo "Après le chien, c'était un en effet un choix possible."
  ;;
  *)
  echo "Faites un effort, vous n'avez pas compris la question."
  ;;
esac
