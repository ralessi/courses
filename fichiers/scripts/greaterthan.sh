#!/bin/bash
if [ $1 -gt 10 ]
then
  echo "$1 est supérieur à 10"
    if [ $1 -gt 100 ]
    then
      echo "et $1 est même supérieur à 100!"
    fi
else
  echo "Il faut aller au-dessus de 10 pour avoir une réponse!"
fi
