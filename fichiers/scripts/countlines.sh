#!/bin/bash

echo "Entrez le nom du fichier dont vous voulez compter les lignes:"
read -p 'Fichier: ' file

numline=0

if [ ! -e $file ] || [ -z $file ]
  then
    echo "Erreur: le fichier $file n'existe pas."
    exit 1
  else
    while read -r line
      # 'let' permet de poser des opérations arithmétiques;
      # à la place, on aurait pu écrire: do ((++numline))
      do let "numline = numline + 1"
    done < $file
fi

if [ $numline -le 1  ]
  then
    echo "Votre fichier $file compte $numline ligne."
  else
    echo "Votre fichier $file compte $numline lignes."
fi
