#!/bin/bash
echo "Veuillez choisir l'extension des fichiers à sauvegarder"
echo "(sans le point):"
read -p 'extension: ' ext
echo "Veuillez choisir le nom du dossier de sauvegarde:"
read -p 'dossier: ' backupdir
if [ -d "$backupdir" ]
  then
  echo "Le dossier $backupdir existe déjà. Veuillez relancer le"
  echo "programme et saisir un autre nom."
  exit 1
elif [ -e "$backupdir".zip ]
  then
  echo "L'archive $backupdir.zip existe déjà. Veuillez la supprimer"
  echo "ou la déplacer en dehors de ce dossier, puis relancez le"
  echo "programme."
  exit 1
else
  mkdir "$backupdir"
fi
cp *.$ext "$backupdir"
nbre=$(ls $backupdir/*.$ext | wc -l)
zip -r "$backupdir".zip "$backupdir"
echo "Terminé. $0 a copié $nbre fichiers .$ext dans $backupdir"
echo "et l'archive $backupdir.zip a été créée."
