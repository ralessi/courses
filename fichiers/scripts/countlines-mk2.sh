#!/bin/bash

echo "Entrez le nom du fichier dont vous voulez compter les lignes:"
read -p 'Fichier: ' file

countlines () {
  cat $1 | wc -l
}

if [ ! -e $file ] || [ -z $file ]
  then
    echo "Erreur: le fichier $file n'existe pas."
    exit 1
  else
    numline=$(countlines $file)
fi

if [ $numline -le 1  ]
  then
    echo "Votre fichier $file compte $numline ligne."
  else
    echo "Votre fichier $file compte $numline lignes."
fi
