#!/bin/bash

read -p 'Combien de fois tirez-vous la queue du lion? ' rahtimes

isnum='^[0-9]+$' # voir le cours sur grep

if ! [[ $rahtimes =~ $isnum ]] # ce type de test ne fait pas partie du
			       # programme
  then
  echo "Erreur: il faut saisir un nombre entier."
  exit 1
fi

# Définition du compteur de rugissements:
rah=1

until [ $rah -gt $rahtimes ]
do
    echo "Raaaaaaaaahhhhhhhhhhhh! ($rah)"
    sleep 1
    ((++rah))
done
