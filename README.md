Comment utiliser ce dépôt git
=============================

Veuillez vous référer à [cette page de notre
wiki](https://notabug.org/ralessi/courses/wiki).

Compilation
===========

Voici la liste des éléments logiciels qu'il faut installer:

1.  [GNU Make](https://www.gnu.org/software/make/)

2.  [rsync](https://rsync.samba.org/)

3.  [pandoc](http://pandoc.org/)

4.  Python (au moins version 2.6 pour Python2 ou 3.3 pour Python3)

5.  Pygments: <http://pygments.org/>

6.  Une distribution dérivée de [TeXLive](http://tug.org/texlive/), à
    savoir:

    1.  Linux: [TeXLive](http://tug.org/texlive/)

    2.  MacOs: [MacTeX](http://tug.org/mactex/)

    3.  Windows: [proTeXt](http://tug.org/protext/)

    **Rem. 1** L'installation doit être *complète*: il faut donc choisir
    l'option *Set installation scheme: **scheme-full***

    **Rem. 2** L'installation doit aussi être *mise à jour* car certains
    fichiers utilisent des *packages* très récents.

Utilisation de la ligne de commande
-----------------------------------

Veuillez vous référer à [cette page de notre
wiki](https://notabug.org/ralessi/courses/wiki/Command_line#utilisation-de-la-ligne-de-commande).

#### Cloner ce dépôt git

    git clone https://gitlab.com/ralessi/courses.git

#### Compiler les fichiers

    make

#### Revenir à la version initiale

    make clean

#### Mise à jour

    git pull
