exts := md png sh bib tex sty
findopts := $(foreach ext,$(exts),-or -iname "*.$(ext)")

all: pdf

pdf: clean
	find ./* -type f | grep -v '/_.*.tex' | grep -v 'sandbox' | grep 'tex$$' > texfiles
	while read l ; do \
		latexmk -quiet -cd -lualatex -e '$$lualatex=q/lualatex %O --shell-escape %S/' $$l ; \
		done < texfiles
	while read l ; do \
		latexmk -c -cd -lualatex $$l ; \
		done < texfiles
	find -type d | grep -i "_minted.*" | xargs rm -rf
clean:
	rm -rf .backup
	find -iname "*~" | xargs rm -rf
	find ./* -type f -iname "makefile" $(findopts) > ls-R
	find ./* -type d -iname "sandbox" >> ls-R
	rsync -avPr --files-from=ls-R . .backup
	rm -rf *
	cp -p -r .backup/* .
	rm -rf .backup
	pandoc -s README.tex -o README.md && sed -i 's/{#.*}//g' README.md && sed -i "s/’/\'/g" README.md
	pandoc -s README.tex -o about.html

.PHONY: all pdf clean
